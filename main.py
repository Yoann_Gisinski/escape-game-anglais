import os, platform

# OS Detection
if platform.system() == "Windows":
    clear = "cls"
else:
    clear = "clear"

# Default Variables
level_list = [1, 2, 3]
key = False
    
# Level Choice
def level_choice(key, level_list, level = int()):
    while level not in level_list:
        os.system(clear)
        print("---- Menu ----")
        print()
        print("What difficulty do you want ?")
        print("Level Medium : 1")
        print("Level Hard : 2")
        print("Level Expert : 3")
        level = input(">>> ")
        if level == "1":
            medium_main_room()
            break
        elif level == "2":
            hard_main_room()
            break
        elif level == "3":
            key = False
            expert_main_room(key)
            break

# Medium Level
def medium_main_room(choix = int()):
    while choix != 1 or choix != 2:
        os.system(clear)
        print("---- Main Room ----")
        print()
        print("There are 2 rooms in front of you :")
        print("- One on the left (1)")
        print("- One on the right (2)")
        print("Which one do you open ?")
        choix = input(">>> ")
        if choix == "1":
            medium_garden()
            break
        elif choix == "2":
            medium_library()
            break

def medium_garden():
    os.system(clear)
    print("---- Garden ----")
    print()
    print("When you go through the left door, you arrive in a beautiful garden.")
    print("There are high walls and no windows or doors to go through.")
    print("You can’t escape from here. You were ready to step back when you noticed there was someone sitting behind a tree.")
    print("You decide to go back to the main room.")
    print("You approach this sad looking man.")
    print("He’s wearing a grey suit and tie.")
    print("He is drawing a very strange creature on a piece of paper.")
    print("It looks like an octopus with wings.")
    print("You try to speak to him, but he doesn’t even seem to notice your presence.")
    print("You decide to go back to the main room.")
    input()
    medium_main_room()
    
def medium_library():
    os.system(clear)
    print("---- Library ----")
    print()
    print("When you go through the right door, you arrive in a beautiful library.")
    print("It is warm in here and you notice that a nice fire is burning in the fireplace.")
    print("In front of it are two dusty armchairs.")
    print("There is a big wooden door in front of you.")
    print("When you have a closer look at the books on the shelf, you notice that some are ancient !")
    print("One of them catches your eye.")
    print("You pick it up and read the title : “The Necronomicon” by Abdul Alhazred.")
    print("You are about to open it when you hear horrible screams coming from behind one of the bookshelves.")
    print("You drop the book on the floor and run for the big wooden door in front of you.")
    print("It opens on the outside of this horrible manor.")
    print("There is a car in front of you.")
    print("All you can do is run to it and pray it will start...")
    input()
    win(key, level_list)
    
# Hard Level
def hard_main_room(choix = int()):
    while choix != 1 or choix != 2 or choix != 3:
        os.system(clear)
        print("---- Main Room ----")
        print()
        print("There are 3 rooms in front of you :")
        print("- One on the left (1)")
        print("- One in the middle (2)")
        print("- One on the right (3)")
        print("Which one do you open ?")
        choix = input(">>> ")
        if choix == "1":
            hard_garden()
            break
        elif choix == "2":
            hard_well()
            break
        elif choix == "3":
            hard_library()
            break

def hard_garden(choix = int()):
    while choix != 1 or choix != 2:
        os.system(clear)
        print("---- Garden ----")
        print()
        print("When you go through the left door, you arrive in a beautiful garden.")
        print("There are high walls and no windows or doors to go through.")
        print("You can’t escape from here.")
        print("You were ready to step back when you noticed there was someone sitting behind a tree.")
        print("You can :")
        print("- Try and talk to him (1)")
        print("- Go back to the main room (2)")
        choix = input(">>> ")
        if choix == "1":
            hard_garden_choice_1()
            break
        elif choix == "2":
            hard_garden_choice_2()
            break
        
def hard_garden_choice_1():
    print()
    print("---- Choice 1 ----")
    print()
    print("You approach this sad looking man.")
    print("He’s wearing a grey suit and tie.")
    print("He is drawing a very strange creature on a piece of paper.")
    print("It looks like an octopus with wings.")
    print("When you speak to him, he looks up and tells you: “Some colours are not meant for humans to be seen”.")
    print("Then, he goes back to his drawing.")
    print("You decide to go back to the main room.")
    input()
    hard_main_room()
    
def hard_garden_choice_2():
    print()
    print("---- Choice 2 ----")
    print()
    print("You decide to go back to the main room.")
    input()
    hard_main_room()
    
def hard_well(choix = int()):
    while choix != 1 or choix != 2:
        os.system(clear)
        print("---- Well ----")
        print()
        print("When you go through the central door, you arrive in a room with a well in the middle.")
        print("There are no windows or doors to go through.")
        print("You can’t escape from here.")
        print("A flash of light emerges suddenly from the well.")
        print("Its colour is impossible to describe.")
        print("You can :")
        print("- Decide to look down in the well (1)")
        print("- Go back to the main room (2)")
        choix = input(">>> ")
        if choix == "1":
            hard_well_choice_1()
            break
        elif choix == "2":
            hard_well_choice_2()
            break
            
def hard_well_choice_1():
    print()
    print("---- Choice 1 ----")
    print()
    print("You approach the well to look inside.")
    print("You bend over it and try to locate the source of the colour but it’s too dark to see anything.")
    print("Another flash of the colour burst and kills you instantly.")
    print("Your body falls in the well where it will rest for eternity.")
    input()
    lose(key, level_list)

def hard_well_choice_2():
    print()
    print("---- Choice 2 ----")
    print()
    print("You decide to go back to the main room.")
    input()
    hard_main_room()
    
def hard_library(choix = int()):
    while choix != 1 or choix != 2:
        os.system(clear)
        print("---- Library ----")
        print()
        print("When you go through the right door, you arrive in a beautiful library.")
        print("It is warm in here and you notice that a nice fire is burning in the fireplace.")
        print("In front of it are two dusty armchairs.")
        print("There is a big wooden door in front of you.")
        print("When you have a closer look at the books on the shelf, you notice that some are ancient !")
        print("One of them catches your eye.")
        print("You pick it up and read the title : “The Necronomicon” by Abdul Alhazred.")
        print("You are about to open it when you hear horrible screams coming from behind one of the bookshelves.")
        print("You put your book back and pushing the books of the shelf where the screams are coming from, you realise there is a hidden door.")
        print("You can :")
        print("- Try to open the hidden door (1)")
        print("- Try to open the big wooden door (2)")
        choix = input(">>> ")
        if choix == "1":
            hard_library_choice_1()
            break
        elif choix == "2":
            hard_library_choice_2()
            break
        
def hard_library_choice_1():
    print()
    print("---- Choice 1 ----")
    print()
    print("The hidden door opens easily.")
    print("What you saw in that room is beyond words.")
    print("Let it suffice to say that it was enough for your heart to stop... definitely.")
    input()
    lose(key, level_list)
    
def hard_library_choice_2():
    print()
    print("---- Choice 2 ----")
    print()
    print("The big wooden door open on the outside of this horrible manor.")
    print("There is a car in front of you.")
    print("All you can do is run to it and pray it will start...")
    input()
    win(key, level_list)
    
# Expert Level
def expert_main_room(key, choix = int()):
    while choix != 1 or choix != 2 or choix != 3:
        os.system(clear)
        print("---- Main Room ----")
        print()
        print("There are 3 rooms in front of you :")
        print("- One on the left (1)")
        print("- One in the middle (2)")
        print("- One on the right (3)")
        print("Which one do you open ?")
        choix = input(">>> ")
        if choix == "1":
            expert_garden(key)
            break
        elif choix == "2":
            expert_well(key)
            break
        elif choix == "3":
            expert_library(key)
            break
            
def expert_garden(key, choix = int()):
    while choix != 1 or choix != 2:
        os.system(clear)
        print("---- Garden ----")
        print()
        print("When you go through the left door, you arrive in a beautiful garden.")
        print("There are high walls and no windows or doors to go through.")
        print("You can’t escape from here.")
        print("You were ready to step back when you noticed there was someone sitting behind a tree.")
        print("You can :")
        print("- Try and talk to him (1)")
        print("- Go back to the main room (2)")
        choix = input(">>> ")
        if choix == "1":
            expert_garden_choice_1(key)
            break
        elif choix == "2":
            expert_garden_choice_2(key)
            break
            
def expert_garden_choice_1(key):
    print()
    print("---- Choice 1 ----")
    print()
    print("You approach this sad looking man.")
    print("He’s wearing a grey suit and tie.")
    print("He is drawing a very strange creature on a piece of paper.")
    print("It looks like an octopus with wings.")
    print("When you speak to him, he looks up and tells you: “Some colours are not meant for humans to be seen”.")
    print("Then, he goes back to his drawing.")
    print("You decide to go back to the main room.")
    input()
    expert_main_room(key)

def expert_garden_choice_2(key):
    print()
    print("---- Choice 2 ----")
    print()
    print("You decide to go back to the main room.")
    input()
    expert_main_room(key)

def expert_well(key, choix = int()):
    while choix != 1 or choix != 2 or choix != 3:
        os.system(clear)
        print("---- Well ----")
        print()
        print("When you go through the central door, you arrive in a room with a well in the middle.")
        print("There are no windows or doors to go through.")
        print("You can’t escape from here.")
        print("A flash of light emerges suddenly from the well.")
        print("Its colour is impossible to describe.")
        print("While the light is on, you notice something shining on the edge of the well.")
        print("You can :")
        print("- Decide to look down in the well (1)")
        print("- Have a look at the shiny thing (2)")
        print("- Go back to the main room (3)")
        choix = input(">>> ")
        if choix == "1":
            expert_well_choice_1(key)
            break
        elif choix == "2":
            expert_well_choice_2(key)
            break
        elif choix == "3":
            expert_well_choice_3(key)
            break

def expert_well_choice_1(key):
    print()
    print("---- Choice 1 ----")
    print()
    print("You approach the well to look inside.")
    print("You bend over it and try to locate the source of the colour but it’s too dark to see anything.")
    print("Another flash of the colour burst and kills you instantly.")
    print("Your body falls in the well where it will rest for eternity.")
    input()
    lose(key, level_list)
    
def expert_well_choice_2(key):
    print()
    print("---- Choice 2 ----")
    print()
    print("The shiny thing is actually a silver key.")
    print("You don’t know what it will open but you decide to take it anyway.")
    key = True
    input()
    expert_well(key)

def expert_well_choice_3(key):
    print()
    print("---- Choice 3 ----")
    print()
    print("You decide to go back to the main room.")
    input()
    expert_main_room(key)

def expert_library(key, choix = int()):
    while choix != 1 or choix != 2:
        os.system(clear)
        print("---- Library ----")
        print()
        print("When you go through the right door, you arrive in a beautiful library.")
        print("It is warm in here and you notice that a nice fire is burning in the fireplace.")
        print("In front of it are two dusty armchairs.")
        print("There is a big wooden door in front of you.")
        print("When you have a closer look at the books on the shelf, you notice that some are ancient !")
        print("One of them catches your eye.")
        print("You pick it up and read the title : “The Necronomicon” by Abdul Alhazred.")
        print("You are about to open it when you hear horrible screams coming from behind one of the bookshelves.")
        print("You put your book back and pushing the books of the shelf where the screams are coming from, you realise there is a hidden door.")
        print("You can :")
        print("- Try to open the hidden door (1)")
        print("- Try to open the big wooden door (2)")
        choix = input(">>> ")
        if choix == "1":
            expert_library_choice_1(key)
            break
        elif choix == "2":
            expert_library_choice_2(key)
            break
        
def expert_library_choice_1(key):
    print()
    print("---- Choice 1 ----")
    print()
    print("The hidden door opens easily.")
    print("What you saw in that room is beyond words.")
    print("Let it suffice to say that it was enough for your heart to stop... definitely.")
    input()
    lose(key, level_list)
    
def expert_library_choice_2(key):
    print()
    print("---- Choice 2 ----")
    print()
    print("The big wooden door is locked.")
    if not key:
        print("There must be a key somewhere to open it.")
        print("You decide to go back to the main room to look after it.")
        input()
        expert_main_room(key)
    else:
        print("You take the key from your pocket and turn it slowly...")
        print("It’s unlocked !")
        print("You push the door that opens on the outside of this horrible manor.")
        print("There is a car in front of you.")
        print("All you can do is run to it and pray it will start...")
        input()
        win(key, level_list)

# Win / Lose functions
def win(key, level_list):
    os.system(clear)
    print("---- You Win ! ----")
    input()
    level_choice(key, level_list)
    
def lose(key, level_list):
    os.system(clear)
    print("---- You are Dead ! ----")
    input()
    level_choice(key, level_list)

level_choice(key, level_list)